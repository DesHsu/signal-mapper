# Signal Mapper #
The purpose of Signal Mapper is to measure both WiFi and Cellular signal quality, of location determined by either GPS signal or pre-defined floorplan.

## Author ##
Macquarie University & Optus

Student: [Yu-Wei Desmond Hsu](mailto:yu-wei.hsu@students.mq.edu.au)

Supervisor: [Michael Heimlich](mailto:michael.heimlich@mq.edu.au), [Steve Cassidy](mailto:steve.cassidy@mq.edu.au)

Industry Supervisor: [Michael Wagg](mailto:Michael.Wagg@optus.com.au)

## APIs and libraries ##
* Android SDK
* Google Map API
* SpeedOf.me API
* mozilla/rhino

## Other resources ##
* floor-plan (Macquarie University)
* 2 Android Smartphones (Optus)

## Documentation ##
[Signal Mapper Wiki](https://bitbucket.org/DesHsu/signal-mapper/wiki/Home)

## License ##
[TODO]