package macquarieuniversityoptus.signalmapper;

import android.content.Context;
import android.graphics.Color;
import android.location.Criteria;
import android.location.Location;
import android.location.LocationManager;
import android.net.ConnectivityManager;
import android.net.wifi.WifiManager;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.telephony.TelephonyManager;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.ProgressBar;
import android.widget.Switch;
import android.widget.TextView;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.Circle;
import com.google.android.gms.maps.model.CircleOptions;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.gson.Gson;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.sql.SQLException;
import java.util.ArrayList;


public class MapsActivity extends FragmentActivity implements GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener {


    private GoogleMap mMap; // Might be null if Google Play services APK is not available.

    private LatLngBounds MQ_NorthRyde = new LatLngBounds(new LatLng(-33.781190, 151.104753), new LatLng(-33.767492, 151.120589));
    private MarkerOptions currentMarkerOption;
    private Marker currentMarker;
    private WaypointDataSource waypointDataSource;
    private WifiManager wifiManager;
    private ConnectivityManager connManager;
    private TelephonyManager telephonyManager;
    private ProgressBar progressBar;
    private TextView progressTextView;
    private String updateMessage="";
    private Button sync_button=null;
    private Button exe_button= null;
    private Button clear_button=null;
    private LocationManager locationManager =null;
    private LocationListener locationListener=null;
    private ArrayList<Waypoint> uploadQueue = new ArrayList<Waypoint>();
    private Switch autoMeasureSwitch = null;
    private Switch wifiOnlySwtich=null;
    private boolean autoMeasure= false;
    private GoogleApiClient mGoogleApiClient;
    private Location mLastLocation;
    private LocationRequest mLocationRequest;
    private boolean onGoingTest= false;
    private boolean wifiOnly=false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_maps);
        locationManager = (LocationManager) this.getSystemService(Context.LOCATION_SERVICE);
        locationListener = new LocationListener() {
            public void onLocationChanged(Location location) {
                // Called when a new location is found by the network location provider.
                if(location.getAccuracy()<30)
                    newLoctionForAutoMeasure(location);

            }

            public void onStatusChanged(String provider, int status, Bundle extras) {}

            public void onProviderEnabled(String provider) {}

            public void onProviderDisabled(String provider) {}
        };
        setUpMapIfNeeded();



        exe_button = (Button) findViewById(R.id.execute_button);
        exe_button.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                if (!onGoingTest) {
                    sync_button.setEnabled(false);
                    exe_button.setEnabled(false);

                    progressBar.setVisibility(ProgressBar.VISIBLE);
                    progressTextView.setVisibility(ProgressBar.VISIBLE);
                    //new thread to measure
                    new AsyncMeasure().execute(currentMarker.getPosition());
                }
            }
        });

        autoMeasureSwitch= (Switch) findViewById(R.id.autoMeasure);
        autoMeasureSwitch.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    autoMeasure = true;

                } else {
                    autoMeasure = false;
                }
            }
        });

        clear_button= (Button)findViewById(R.id.clearDB_button);
        clear_button.setOnClickListener(new View.OnClickListener(){

            @Override
            public void onClick(View v) {
                waypointDataSource.dropTable();
                loadMarkersFromLocalDB();
            }
        });

        wifiOnlySwtich= (Switch) findViewById(R.id.wifiOnly);
        wifiOnlySwtich.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                wifiOnly=isChecked;
            }
        });


        sync_button = (Button) findViewById(R.id.sync_button);
        sync_button.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {

                sync_button.setText("Syncing...");
                exe_button.setEnabled(false);
                sync_button.setEnabled(false);

                //upload waypoints in queue
                Waypoint[] queue= new Waypoint[uploadQueue.size()];
                for(int i=0;i<uploadQueue.size();i++){
                    queue[i]= uploadQueue.get(i);
                }
                new UploadWaypoint().execute(queue);

                //when uploaded successfully, we'll attempt to download it again.
                //if download is successful, we'll clear the map and add the markers
                if(uploadQueue.size()==0) {

                new DownloadWaypoint().execute();




                }



            }
        });




        waypointDataSource= new WaypointDataSource(this.getApplicationContext());
        try {
            waypointDataSource.open();
        } catch (SQLException e) {

            e.printStackTrace();
        }
        loadMarkersFromLocalDB();






        wifiManager = (WifiManager)getSystemService(Context.WIFI_SERVICE);
        connManager = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        telephonyManager = (TelephonyManager)getSystemService(Context.TELEPHONY_SERVICE);
        progressBar= (ProgressBar)findViewById(R.id.progressBar);
        progressTextView= (TextView)findViewById(R.id.progressTextView);
        progressBar.setVisibility(ProgressBar.INVISIBLE);
        progressTextView.setVisibility(TextView.INVISIBLE);
        progressTextView.setText("");



        buildGoogleApiClient();
        mGoogleApiClient.connect();

    }

    protected synchronized void buildGoogleApiClient() {
        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .addApi(LocationServices.API)
                .build();
    }



    @Override
    protected void onResume() {
        super.onResume();
        setUpMapIfNeeded();

    }

    /**
     * Sets up the map if it is possible to do so (i.e., the Google Play services APK is correctly
     * installed) and the map has not already been instantiated.. This will ensure that we only ever
     * call {@link #setUpMap()} once when {@link #mMap} is not null.
     * <p/>
     * If it isn't installed {@link SupportMapFragment} (and
     * {@link com.google.android.gms.maps.MapView MapView}) will show a prompt for the user to
     * install/update the Google Play services APK on their device.
     * <p/>
     * A user can return to this FragmentActivity after following the prompt and correctly
     * installing/updating/enabling the Google Play services. Since the FragmentActivity may not
     * have been completely destroyed during this process (it is likely that it would only be
     * stopped or paused), {@link #onCreate(Bundle)} may not be called again so we should call this
     * method in {@link #onResume()} to guarantee that it will be called.
     */
    private void setUpMapIfNeeded() {
        // Do a null check to confirm that we have not already instantiated the map.
        if (mMap == null) {
            // Try to obtain the map from the SupportMapFragment.
            mMap = ((SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.map))
                    .getMap();
            // Check if we were successful in obtaining the map.
            if (mMap != null) {
                setUpMap();
            }
        }
    }

    /**
     * This is where we can add markers or lines, add listeners or move the camera. In this case, we
     * just add a marker near Africa.
     * <p/>
     * This should only be called once and when we are sure that {@link #mMap} is not null.
     */
    private void setUpMap() {
        //Add marker for current position
        currentMarkerOption = new MarkerOptions().position(new LatLng(-33.773913, 151.112607)).title("Current Position");
        currentMarkerOption.draggable(true);
        setUpMainMarker();

        mMap.setOnMapLongClickListener(new GoogleMap.OnMapLongClickListener() {

            @Override
            public void onMapLongClick(LatLng point) {
                currentMarker.setPosition(point);
            }
        });
        mMap.setMyLocationEnabled(false);
        mMap.getUiSettings().setMapToolbarEnabled(false);
        mMap.getUiSettings().setCompassEnabled(true);
        mMap.getUiSettings().setMyLocationButtonEnabled(true);
        mMap.getUiSettings().setTiltGesturesEnabled(false);

        final LocationManager locationManager = (LocationManager) getSystemService(LOCATION_SERVICE);

        Criteria criteria = new Criteria();
        criteria.setAccuracy(Criteria.ACCURACY_FINE);




        // Getting initial Location
        //Location location = locationManager.getLastKnownLocation(provider);
        // Show the initial location
        /**
        mMap.setOnMyLocationButtonClickListener(new GoogleMap.OnMyLocationButtonClickListener() {

            @Override
            public boolean onMyLocationButtonClick() {

                //if (checkSelfPermission(Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && checkSelfPermission(Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                    // TODO: Consider calling
                    //    public void requestPermissions(@NonNull String[] permissions, int requestCode)
                    // here to request the missing permissions, and then overriding
                    //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
                    //                                          int[] grantResults)
                    // to handle the case where the user grants the permission. See the documentation
                    // for Activity#requestPermissions for more details.
                    //return TODO;
                //}



                return true;
            }
        });
        */

        mMap.setOnMarkerDragListener(new GoogleMap.OnMarkerDragListener() {

            @Override
            public void onMarkerDragStart(Marker M0) {
            }

            @Override
            public void onMarkerDragEnd(Marker M0) {
            }

            @Override
            public void onMarkerDrag(Marker M0) {

            }
        });



        mMap.moveCamera(CameraUpdateFactory.newLatLngBounds(MQ_NorthRyde, this.getResources().getDisplayMetrics().widthPixels,
                this.getResources().getDisplayMetrics().heightPixels, 50));


    }

    private void newLoctionForAutoMeasure(Location location) {
        if(autoMeasure){
            LatLng newLocation=new LatLng(location.getLatitude(),location.getLongitude());
            ArrayList<Waypoint> waypointsNearby = waypointDataSource.getWaypointsNearby(newLocation);
            Log.d("Nearbys", ""+waypointsNearby.size());
            if(waypointsNearby.size()==0) {
                currentMarkerOption.position(newLocation);
                currentMarker.setPosition(newLocation);
                CameraUpdate cameraUpdate = CameraUpdateFactory.newLatLngZoom(newLocation, 100);
                mMap.animateCamera(cameraUpdate);
                exe_button.performClick();
            }
        }
    }

    private void setUpMainMarker() {
        currentMarker = mMap.addMarker(currentMarkerOption);
        //When marker is dragged
        currentMarker.showInfoWindow();
        currentMarker.setTitle("Current Position");
        currentMarker.setSnippet("hold and drag");
    }

    @Override
    public void onConnected(Bundle bundle) {
        mLastLocation = LocationServices.FusedLocationApi.getLastLocation(
                mGoogleApiClient);
        if (mLastLocation != null) {
            if(autoMeasure){
                newLoctionForAutoMeasure(mLastLocation);
            }
        }
        createLocationRequest();
        LocationServices.FusedLocationApi.requestLocationUpdates(mGoogleApiClient, mLocationRequest, locationListener);


    }
    protected void createLocationRequest() {

        mLocationRequest = new LocationRequest();
        mLocationRequest.setInterval(5000);
        mLocationRequest.setFastestInterval(  1000);
        mLocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
    }

    @Override
    public void onConnectionSuspended(int i) {

    }

    @Override
    public void onConnectionFailed(ConnectionResult connectionResult) {

    }


    public class AsyncMeasure extends AsyncTask<LatLng, Integer, Waypoint> {
        static final int WAITING_WIFI_CONNECTION= 0;
        static final int WAITING_WIFI_PING=1;
        static final int WAITING_MOBILE_CONNECTION=2;
        static final int WAITING_MOBILE_PING=3;

        protected Waypoint doInBackground(LatLng... latLng) {
            onGoingTest=true;


            //new measurement
            Waypoint waypoint = new Waypoint(latLng[0], wifiManager,connManager, telephonyManager , wifiOnly, this);
            return waypoint;
        }

        protected void onProgressUpdate(Integer... progress) {
            Log.d("Progress", ""+progress[0]);

            switch (progress[0]) {
                case WAITING_WIFI_CONNECTION:
                    updateMessage="Connecting WiFi...";
                    break;
                case WAITING_MOBILE_CONNECTION:
                    updateMessage="Connecting mobile network...";
                    break;
                case WAITING_WIFI_PING:
                    updateMessage="WiFi latency test...";
                    break;
                case WAITING_MOBILE_PING:
                    updateMessage="Mobile latency test...";
                    break;
            }
            runOnUiThread(
                    new Runnable() {
                        @Override
                        public void run() {
                            progressTextView.setText(updateMessage);
                        }
                    }
            );

        }

        protected void onPostExecute(Waypoint result) {

            //keep it in upload queue
            uploadQueue.add(result);
            //save to local db
            waypointDataSource.insertWaypoints(result);
            //clear and make marker from db
            loadMarkersFromLocalDB();
            //stop processing screen
            progressBar.setVisibility(ProgressBar.INVISIBLE);
            progressTextView.setVisibility(ProgressBar.INVISIBLE);
            updateSyncButton();

            //locationManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, 0, 0, locationListener);
            //Location lastKnownLocation = locationManager.getLastKnownLocation(locationManager.getBestProvider(new Criteria(), true));

            onGoingTest=false;
        }

    }
    private class UploadWaypoint extends AsyncTask<Waypoint, Integer, Integer> {
        protected Integer doInBackground(Waypoint... results) {
            ArrayList<Waypoint> unsuccessful = new ArrayList<Waypoint>();
            HttpURLConnection urlConnection = null;
            String charset = "utf-8";

            //set URL
            URL url = null;
            try {
                url = new URL("https://sheetsu.com/apis/6874b43f");
            } catch (MalformedURLException e) {
                e.printStackTrace();
            }
            Gson gson = new Gson();
            int numOfSuccessfulUpdates=0;
            for(int i=0;i<results.length;i++) {
                //remove from upload queue


                //set urlConnection

                try {
                    urlConnection = (HttpURLConnection)url.openConnection();
                    urlConnection.setDoOutput(true); // Triggers POST.
                    urlConnection.setRequestMethod("POST");
                    urlConnection.setChunkedStreamingMode(0);
                    urlConnection.setRequestProperty("Content-Type", "application/json; charset="+charset);

                    OutputStream out = urlConnection.getOutputStream();
                    out.write(gson.toJson(results[i]).getBytes());
                    out.flush();

                    //CHECK DATA THAT IS UPLOADED
                    //Log.d("Upload", "URL: " + url);
                    //Log.d("Upload", "Method: " + urlConnection.getRequestMethod());
                    //Log.d("Upload", "Charset: "+ urlConnection.getRequestProperty("Accept-Charset"));
                    //Log.d("Upload", "Content-Type: " + urlConnection.getRequestProperty("Content-Type"));
                    //Log.d("Upload", "JSON: " + gson.toJson(results[i]));

                    out.close();

                } catch (IOException e) {
                    e.printStackTrace();
                }

                //keep a record of number of waypont saved
                try {
                    //Log.d("Upload","Status: "+urlConnection.getResponseCode());
                    if(urlConnection.getResponseCode()==201){
                        //success!
                        numOfSuccessfulUpdates++;
                    }else{
                        //failure! Try later!
                        unsuccessful.add(results[i]);
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                }finally{
                    urlConnection.disconnect();
                }

            }
            uploadQueue=unsuccessful;

            return numOfSuccessfulUpdates;
        }

        protected void onProgressUpdate(Integer... progress) {

        }

        protected void onPostExecute(Integer result) {
            Log.d("Upload","Number of waypoints uploaded with status 201: "+result);
            updateSyncButton();
        }
    }


    private class DownloadWaypoint extends AsyncTask<Void, Integer, ArrayList<Waypoint>> {
        protected ArrayList<Waypoint> doInBackground(Void... params) {

            ArrayList<Waypoint> waypoints= new ArrayList<Waypoint>();
            //download waypoints from spreadsheet and add to local DB
                    /*-------------------------------DOWNLOADING DATA FROM SPREADSHEET---------------------------*/
            URL url = null;
            String charset = "utf-8";
            try {
                url = new URL("https://sheetsu.com/apis/6874b43f");
            } catch (MalformedURLException e) {
                e.printStackTrace();
            }
            Gson gson = new Gson();
            //set urlConnection
            HttpURLConnection urlConnection = null;
            try {
                urlConnection = (HttpURLConnection)url.openConnection();
                urlConnection.setRequestMethod("GET");
                urlConnection.setChunkedStreamingMode(0);
                urlConnection.setRequestProperty("Content-Type", "application/json; charset=" + charset);


                if(urlConnection.getResponseCode()==200){
                    //success!



                    waypointDataSource.dropTable();
                    //add all waypoints to localDB
                            /*---------------Read json-----------------*/
                    BufferedReader r = new BufferedReader(new InputStreamReader(urlConnection.getInputStream()));
                    StringBuilder json = new StringBuilder();
                    String line;
                    while ((line = r.readLine()) != null) {
                        json.append(line);
                    }
                    String jsonString= json.toString();
                    Log.d("Download",jsonString);

                            /*--------------Separate waypoints-------------------*/
                    ArrayList<String> waypointsJson= new ArrayList<String>();

                    //cut off rubbish
                    int waypointStart=jsonString.indexOf("{\"LAT\"");
                    int waypointEnd=jsonString.indexOf("}");

                    if(waypointStart>=0) {
                        jsonString = jsonString.substring(waypointStart, jsonString.length());
                        //Log.d("Download", jsonString);

                        waypointStart = jsonString.indexOf("{\"LAT\"");
                        waypointEnd = jsonString.indexOf("}");
                    }
                    while(waypointStart>=0&&waypointEnd>=0){

                        String waypointString = jsonString.substring(waypointStart,waypointEnd+1);


                        waypoints.add(gson.fromJson(waypointString, Waypoint.class));


                        jsonString= jsonString.substring(waypointEnd+1, jsonString.length());
                        waypointStart=jsonString.indexOf("{\"LAT\"");
                        waypointEnd=jsonString.indexOf("}");
                    }

                }else{
                    //failure!
                }

            } catch (IOException e) {
                e.printStackTrace();
            }finally{
                urlConnection.disconnect();
            }

            /*-------------------------------FINISHED DOWNLOADING DATA FROM SPREADSHEET---------------------------*/

            return waypoints;
        }

        protected void onProgressUpdate(Integer... progress) {

        }

        protected void onPostExecute(ArrayList<Waypoint> waypointsList) {
            //waypoints to localDB
            Waypoint[] waypints= new Waypoint[waypointsList.size()];
            for(int i=0;i<waypointsList.size();i++){
                waypints[i]= waypointsList.get(i);
            }
            waypointDataSource.insertWaypoints(waypints);

            //load markers
            loadMarkersFromLocalDB();
            updateSyncButton();

        }
    }

    private void updateSyncButton() {
        //do this after measure or sync is finished
        exe_button.setEnabled(true);
        sync_button.setEnabled(true);
        sync_button.setText("Sync Now (" + uploadQueue.size() + ")");
    }


    public void saveWaypointToDB(Waypoint... waypoints){

        //save to local DB
        waypointDataSource.insertWaypoints(waypoints);

    }

    private void loadMarkersFromLocalDB() {
        ArrayList < Waypoint > results = waypointDataSource.getAllWaypoints();
        mMap.clear();
        for(int i=0;i<results.size();i++){
            Waypoint waypoint= results.get(i);
            makeHistoryMarker(waypoint);
        }

        currentMarkerOption.position(currentMarker.getPosition());
        setUpMainMarker();
    }

    private void loadMarkersFromQueue() {
        Waypoint[] waypoints= new Waypoint[uploadQueue.size()];
        for(int i=0;i<uploadQueue.size();i++){
            waypoints[i]=uploadQueue.get(i);
        }
        makeHistoryMarker(waypoints);
    }

    public void makeHistoryMarker(Waypoint... waypoints){
        for(int i=0;i<waypoints.length;i++) {
            int color = Color.argb(255,0,0,0);
            if(waypoints[i].get("TELE_PING")==-1&& waypoints[i].get("TELE_STRENGTH")==-1){
                color= Color.argb((int)waypoints[i].get("WLAN_PING")==-1?0:150-(int)waypoints[i].get("WLAN_PING"), 0, 0,255);
            }
            if(waypoints[i].get("WLAN_PING")==-1&& waypoints[i].get("WIFI_STRENGTH")==-1){

                color= Color.argb(((int)waypoints[i].get("TELE_PING")==-1?0:150-(int)waypoints[i].get("TELE_PING")), 255, 0,0);
            }
            int strokeColor= Color.argb(100,0,0,0);
            CircleOptions circleOptions = new CircleOptions()
                    .center(new LatLng(waypoints[i].get("LAT"),waypoints[i].get("LONG")))
                    .radius(5.566)
                    .strokeColor(strokeColor)
                    .strokeWidth(2)
                    .fillColor(color);// In meters

            // Get back the mutable Circle
            Circle circle = mMap.addCircle(circleOptions);


            /*-----------------------Add markers instead of circles (data included in the snippet)------------------------------
            //add a 'history' marker at the currentMarker position
            String result = "";
            double[] results = waypoints[i].getAll();
            String[] results_names = WaypointSQLiteHelper.COLUMN_IDs;
            for (int j = 3; j < results.length; j++) {

                if (results[j] > 0 || j == 9 || j == 10) {
                    if (!result.isEmpty()) {
                        result += ", ";
                    }
                    result += results_names[j] + ": " + results[j];
                }

            }
            Log.d("Making a marker", result);
            MarkerOptions history = new MarkerOptions().position(new LatLng(waypoints[i].get("LAT"), waypoints[i].get("LONG"))).title(waypoints[i].get("LAT") + ", " + waypoints[i].get("LONG")).snippet(result);
            history.alpha((float) 0.2);
            history.icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_AZURE));
            history.draggable(false);


            mMap.addMarker(history);
            */
        }
    }






}