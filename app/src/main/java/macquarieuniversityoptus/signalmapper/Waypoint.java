package macquarieuniversityoptus.signalmapper;


import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import android.telephony.CellInfo;
import android.telephony.CellInfoCdma;
import android.telephony.CellInfoGsm;
import android.telephony.CellInfoLte;
import android.telephony.CellSignalStrengthCdma;
import android.telephony.CellSignalStrengthGsm;
import android.telephony.CellSignalStrengthLte;
import android.telephony.TelephonyManager;
import android.util.Log;

import com.google.android.gms.maps.model.LatLng;

import java.io.IOException;


/**
 * Created by Des on 26-Sep-15.
 */
public class Waypoint {


    //Speed test is not implemented yet. All fields will be recorded on local DB and remote. Just change field.
    private double LAT;
    private double LONG;
    private double STOREY;

    private double WLAN_DLSPEED=-1;
    private double WLAN_ULSPEED=-1;
    private double WLAN_PING=-1;
    private double WIFI_STRENGTH=-1;

    private double TELE_DLSPEED=-1;
    private double TELE_ULSPEED=-1;
    private double TELE_PING=-1;
    private double TELE_STRENGTH=-1;

    Waypoint(){

    }
    @Override
    public String toString() {
        Log.d("toString","DataObject [LAT=" + LAT +
                ", LONG=" + LONG +
                ", STOREY=" + STOREY +
                ", WLAN_DLSPEED=" + WLAN_DLSPEED +
                ", WLAN_ULSPEED=" + WLAN_ULSPEED +
                ", WLAN_PING=" + WLAN_PING +
                ", WIFI_STRENGTH=" + WIFI_STRENGTH +
                ", TELE_DLSPEED=" + TELE_DLSPEED +
                ", TELE_ULSPEED=" + TELE_PING +
                ", TELE_STRENGTH=" + TELE_STRENGTH +"]");
        return "DataObject [LAT=" + LAT +
                ", LONG=" + LONG +
                ", STOREY=" + STOREY +
                ", WLAN_DLSPEED=" + WLAN_DLSPEED +
                ", WLAN_ULSPEED=" + WLAN_ULSPEED +
                ", WLAN_PING=" + WLAN_PING +
                ", WIFI_STRENGTH=" + WIFI_STRENGTH +
                ", TELE_DLSPEED=" + TELE_DLSPEED +
                ", TELE_ULSPEED=" + TELE_PING +
                ", TELE_STRENGTH=" + TELE_STRENGTH +"]";
    }

    Waypoint(LatLng latLng, WifiManager wifiManager, ConnectivityManager connManager, TelephonyManager telephonyManager,boolean wifiOnly, MapsActivity.AsyncMeasure asyncMeasure){
        //LatLng is here to specify location of a measurement
        //WiFiManager, ConnectivityManager, and TelephonyManager is parsed here to avoid recreating a WiFiManager for each measurement
        //AsyncMeasure parsed here in order to make updates to the UI(e.g. stuck on WiFi ping... etc.)
        NetworkInfo mobileNetworkInfo= connManager.getNetworkInfo(ConnectivityManager.TYPE_MOBILE);
        NetworkInfo networkInfo = connManager.getNetworkInfo(ConnectivityManager.TYPE_WIFI);
        this.LAT= latLng.latitude;
        this.LONG=latLng.longitude;

        int trialCount=0;
        //Wait till wifi is connected, then test

        if(wifiOnly) {
            asyncMeasure.onProgressUpdate(MapsActivity.AsyncMeasure.WAITING_WIFI_CONNECTION);
            wifiManager.setWifiEnabled(true);
            wifiManager.reconnect();

            while (!networkInfo.isConnected() && trialCount < 5) {
                //wait till it is connected


                try {
                    Thread.currentThread().sleep(100);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                if (wifiManager.disconnect()) {
                    wifiManager.setWifiEnabled(true);
                    wifiManager.reconnect();
                }
                trialCount++;
            }
            trialCount = 0;
            asyncMeasure.onProgressUpdate(MapsActivity.AsyncMeasure.WAITING_WIFI_PING);
            while (networkInfo.isConnected() && WLAN_PING <= 0 && trialCount < 1) {

                //if after 5 trials to connect, and still fail, don't try to ping
                WLAN_PING = ping();
                trialCount++;
            }
            WIFI_STRENGTH = wifiStrengthTest(wifiManager);

        }


        if(!wifiOnly) {
            asyncMeasure.onProgressUpdate(MapsActivity.AsyncMeasure.WAITING_MOBILE_CONNECTION);
            // After WiFi connection is tested. By turning off wifi, we can use mobile data
            wifiManager.setWifiEnabled(false);


            //Wait till mobile is connected, then test
            trialCount = 0;


            while ((!mobileNetworkInfo.isConnected() || networkInfo.isConnected()) && trialCount < 5) {
                try {
                    Thread.currentThread().sleep(100);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                wifiManager.disconnect();
                wifiManager.setWifiEnabled(false);
                trialCount++;
            }
            trialCount = 0;
            asyncMeasure.onProgressUpdate(MapsActivity.AsyncMeasure.WAITING_MOBILE_PING);
            while (TELE_PING <= 0 && mobileNetworkInfo.isConnected() && !networkInfo.isConnected() && trialCount < 1) {

                //if after 5 trials to connect, and still fail, don't try to ping
                TELE_PING = ping();
                trialCount++;
            }
            TELE_STRENGTH = mobileStrengthTest(telephonyManager);


        }

    }

    private double mobileStrengthTest(TelephonyManager telephonyManager) {
        double result=0;

        if(telephonyManager.getAllCellInfo()!=null) {
            for (final CellInfo info : telephonyManager.getAllCellInfo()) {
                if (info instanceof CellInfoGsm) {
                    final CellSignalStrengthGsm gsm = ((CellInfoGsm) info).getCellSignalStrength();
                    result = gsm.getDbm();
                    // do what you need
                } else if (info instanceof CellInfoCdma) {
                    final CellSignalStrengthCdma cdma = ((CellInfoCdma) info).getCellSignalStrength();
                    result = cdma.getCdmaDbm();
                    // do what you need
                } else if (info instanceof CellInfoLte) {
                    final CellSignalStrengthLte lte = ((CellInfoLte) info).getCellSignalStrength();
                    result = lte.getDbm();
                }
            }
        }else{
            return -1;
        }
        return result;
    }

    private double wifiStrengthTest(WifiManager wifiManager) {

        WifiInfo wifiInfo = wifiManager.getConnectionInfo();
        return wifiInfo.getRssi();
    }




    private double ping() {
        long startTime = System.currentTimeMillis();
        Process p1 = null;
        try {
            p1 = Runtime.getRuntime().exec("ping -c 1 www.google.com");
        } catch (IOException e) {
            Log.d("Ping", "error while pinging");
            e.printStackTrace();
        }

        long duration = System.currentTimeMillis() - startTime;
        int returnVal = 0;
        try {
            returnVal = p1.waitFor();
        } catch (InterruptedException e) {
            Log.d("Ping", "InterruptedException when getting ping value");
            e.printStackTrace();
        }

        boolean reachable = (returnVal == 0);
        if (reachable) {
            return duration;
        } else {
            return -1;
        }

    }
    /*public static void setMobileNetworkFromLollipop(Context context) throws Exception {
        String command = null;
        int state = 0;
        try {
            // Get the current state of the mobile network.
            state = isMobileDataEnabledFromLollipop(context) ? 0 : 1;
            // Get the value of the "TRANSACTION_setDataEnabled" field.
            String transactionCode = getTransactionCode(context);
            // Android 5.1+ (API 22) and later.
            if (Build.VERSION.SDK_INT > Build.VERSION_CODES.LOLLIPOP) {
                SubscriptionManager mSubscriptionManager = (SubscriptionManager) context.getSystemService(Context.TELEPHONY_SUBSCRIPTION_SERVICE);
                // Loop through the subscription list i.e. SIM list.
                for (int i = 0; i < mSubscriptionManager.getActiveSubscriptionInfoCountMax(); i++) {
                    if (transactionCode != null && transactionCode.length() > 0) {
                        // Get the active subscription ID for a given SIM card.
                        int subscriptionId = mSubscriptionManager.getActiveSubscriptionInfoList().get(i).getSubscriptionId();
                        // Execute the command via `su` to turn off
                        // mobile network for a subscription service.
                        command = "service call phone " + transactionCode + " i32 " + subscriptionId + " i32 " + state;
                        executeCommandViaSu(context, "-c", command);
                    }
                }
            } else if (Build.VERSION.SDK_INT == Build.VERSION_CODES.LOLLIPOP) {
                // Android 5.0 (API 21) only.
                if (transactionCode != null && transactionCode.length() > 0) {
                    // Execute the command via `su` to turn off mobile network.
                    command = "service call phone " + transactionCode + " i32 " + state;
                    executeCommandViaSu(context, "-c", command);
                }
            }
        } catch(Exception e) {
            // Oops! Something went wrong, so we throw the exception here.
            throw e;
        }
    }
    private static void executeCommandViaSu(Context context, String option, String command) {
        boolean success = false;
        String su = "su";
        for (int i=0; i < 3; i++) {
            // Default "su" command executed successfully, then quit.
            if (success) {
                break;
            }
            // Else, execute other "su" commands.
            if (i == 1) {
                su = "/system/xbin/su";
            } else if (i == 2) {
                su = "/system/bin/su";
            }
            try {
                // Execute command as "su".
                Runtime.getRuntime().exec(new String[]{su, option, command});
            } catch (IOException e) {
                success = false;
                // Oops! Cannot execute `su` for some reason.
                // Log error here.
            } finally {
                success = true;
            }
        }
    }
    private static boolean isMobileDataEnabledFromLollipop(Context context) {
        boolean state = false;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            state = Settings.Global.getInt(context.getContentResolver(), "mobile_data", 0) == 1;
        }
        return state;
    }
    private static String getTransactionCode(Context context) throws Exception {
        try {
            final TelephonyManager mTelephonyManager = (TelephonyManager) context.getSystemService(Context.TELEPHONY_SERVICE);
            final Class<?> mTelephonyClass = Class.forName(mTelephonyManager.getClass().getName());
            final Method mTelephonyMethod = mTelephonyClass.getDeclaredMethod("getITelephony");
            mTelephonyMethod.setAccessible(true);
            final Object mTelephonyStub = mTelephonyMethod.invoke(mTelephonyManager);
            final Class<?> mTelephonyStubClass = Class.forName(mTelephonyStub.getClass().getName());
            final Class<?> mClass = mTelephonyStubClass.getDeclaringClass();
            final Field field = mClass.getDeclaredField("TRANSACTION_setDataEnabled");
            field.setAccessible(true);
            return String.valueOf(field.getInt(null));
        } catch (Exception e) {
            // The "TRANSACTION_setDataEnabled" field is not available,
            // or named differently in the current API level, so we throw
            // an exception and inform users that the method is not available.
            throw e;
        }
    }*/
    public double[] getAll(){
        double[] result= {LAT,LONG,STOREY,WLAN_PING,WLAN_DLSPEED,WLAN_ULSPEED,TELE_PING,TELE_DLSPEED,TELE_ULSPEED,WIFI_STRENGTH,TELE_STRENGTH};
        return result;
    }

    public double get(String valueName){
        switch(valueName){
            case "LAT":
                return LAT;
            case "LONG":
                return LONG;
            case "STOREY":
                return STOREY;
            case "WLAN_DLSPEED":
                return WLAN_DLSPEED;
            case "WLAN_ULSPEED":
                return WLAN_ULSPEED;
            case "WLAN_PING":
                return WLAN_PING;
            case "WIFI_STRENGTH":
                return WIFI_STRENGTH;
            case "TELE_PING":
                return TELE_PING;
            case "TELE_DLSPEED":
                return TELE_DLSPEED;
            case "TELE_ULSPEED":
                return TELE_ULSPEED;
            case "TELE_STRENGTH":
                return TELE_STRENGTH;
        }
        return (double)-2;
    }
    public void Set(String valueName, Double value){
        switch(valueName){
            case "LAT":
                LAT= value;
                break;
            case "LONG":
                LONG= value;
                break;
            case "STOREY":
                STOREY= value;
                break;
            case "WLAN_DLSPEED":
                WLAN_DLSPEED= value;
                break;
            case "WLAN_ULSPEED":
                WLAN_ULSPEED= value;
                break;
            case "WLAN_PING":
                WLAN_PING= value;
                break;
            case "WIFI_STRENGTH":
                WIFI_STRENGTH= value;
                break;
            case "TELE_PING":
                TELE_PING= value;
                break;
            case "TELE_DLSPEED":
                TELE_DLSPEED= value;
                break;
            case "TELE_ULSPEED":
                TELE_ULSPEED= value;
                break;
            case "TELE_STRENGTH":
                TELE_STRENGTH= value;
                break;
        }
    }
}