package macquarieuniversityoptus.signalmapper;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import com.google.android.gms.maps.model.LatLng;

import java.sql.SQLException;
import java.util.ArrayList;

/**
 * Created by Des on 05-Oct-15.
 */


public class WaypointDataSource {
    private SQLiteDatabase database;
    private WaypointSQLiteHelper dbHelper;
    private String[] allColumns = WaypointSQLiteHelper.COLUMN_IDs;

    public WaypointDataSource(Context context){
        dbHelper = new WaypointSQLiteHelper(context);
    }
    public void open() throws SQLException {
        database = dbHelper.getWritableDatabase();
        dbHelper.setDB(database);
    }

    public void close() {
        dbHelper.close();
    }

    public int insertWaypoints(Waypoint... waypoints){
        for(int i=0;i<waypoints.length;i++) {
            ContentValues values = new ContentValues();
            for (int j = 0; j < allColumns.length; j++) {
                values.put(allColumns[j], waypoints[i].get(allColumns[j]));
            }
            long insertID = database.insert(WaypointSQLiteHelper.TABLE_WAYPOINTS, null, values);

        }
        return waypoints.length;
    }

    public boolean deleteWaypoint(Waypoint waypoint){
        return false;
    }

    public ArrayList<Waypoint> getAllWaypoints(){
        Cursor cursor = database.query(WaypointSQLiteHelper.TABLE_WAYPOINTS,WaypointSQLiteHelper.COLUMN_IDs,"", null,"","","");
        ArrayList<Waypoint> results= cursorToWaypoints(cursor);
        return results;
    }

    public ArrayList<Waypoint> getWaypointsNearby(LatLng latLng){
        double latUpper=latLng.latitude+0.00005;
        double latLower= latLng.latitude-0.00005;
        double longUpper=latLng.longitude+0.00005;
        double longLower= latLng.longitude-0.00005;
        String[] whereArgs= new String[]{latUpper+"",latLower+"",longUpper+"", longLower+""};
        Cursor cursor = database.query(WaypointSQLiteHelper.TABLE_WAYPOINTS,WaypointSQLiteHelper.COLUMN_IDs,"LAT <= ? AND  LAT >=? AND LONG <= ? AND LONG>= ?", whereArgs,"","","");
        ArrayList<Waypoint> nearbyWaypoints= cursorToWaypoints(cursor);
        return nearbyWaypoints;
     }

    public void dropTable(){
        database.execSQL("DROP TABLE IF EXISTS " + WaypointSQLiteHelper.TABLE_WAYPOINTS);
        dbHelper.recreateTableIfNeeded();
    }

    private ArrayList<Waypoint> cursorToWaypoints(Cursor cursor){
        cursor.moveToFirst();
        ArrayList<Waypoint> waypoints= new ArrayList<Waypoint>();
        for(int j=0;j<cursor.getCount();j++) {
            Waypoint waypoint = new Waypoint();
            for (int i = 0; i < allColumns.length; i++){
                waypoint.Set(allColumns[i], (double) cursor.getDouble(i));

            }
            waypoints.add(waypoint);
            cursor.moveToNext();
        }
        return waypoints;
    }


}
