package macquarieuniversityoptus.signalmapper;



import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;
/**
 * Created by Des on 05-Oct-15.
 */


public class WaypointSQLiteHelper extends SQLiteOpenHelper {

    public SQLiteDatabase db;
    public static final String TABLE_WAYPOINTS = "waypoints";
    public static final String[] COLUMN_IDs={"LAT","LONG","STOREY","WLAN_PING","WLAN_DLSPEED","WLAN_ULSPEED",
            "TELE_PING","TELE_DLSPEED","TELE_ULSPEED","WIFI_STRENGTH","TELE_STRENGTH"
    };

    private static final String DATABASE_NAME = "waypoints.db";
    private static final int DATABASE_VERSION = 1;

    String sql = "CREATE TABLE IF NOT EXISTS " + TABLE_WAYPOINTS +
            " ("+COLUMN_IDs[0]+"          NUMERIC      NOT NULL, " +
            COLUMN_IDs[1]+"               NUMERIC      NOT NULL, " +
            COLUMN_IDs[2]+"               NUMERIC      NOT NULL, " +
            COLUMN_IDs[3]+"               NUMERIC      NOT NULL, " +
            COLUMN_IDs[4]+"               NUMERIC      NOT NULL, " +
            COLUMN_IDs[5]+"               NUMERIC      NOT NULL, " +
            COLUMN_IDs[6]+"               NUMERIC      NOT NULL, " +
            COLUMN_IDs[7]+"               NUMERIC      NOT NULL, " +
            COLUMN_IDs[8]+"               NUMERIC      NOT NULL, " +
            COLUMN_IDs[9]+"               NUMERIC      NOT NULL, " +
            COLUMN_IDs[10]+"              NUMERIC      NOT NULL) ";

    public WaypointSQLiteHelper(Context context) {

        super(context, DATABASE_NAME, null, DATABASE_VERSION);

    }

    @Override
    public void onCreate(SQLiteDatabase database) {

        database.execSQL(sql);
        Log.d("db", "created new table");
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        Log.w(WaypointSQLiteHelper.class.getName(),
                "Upgrading database from version " + oldVersion + " to "
                        + newVersion + ", which will destroy all old data");
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_WAYPOINTS);
        onCreate(db);
    }

    public void setDB(SQLiteDatabase DB) {
        this.db = DB;
        recreateTableIfNeeded();
    }

    public void recreateTableIfNeeded() {
        db.execSQL(sql);
        Log.d("db","executed");
    }
}